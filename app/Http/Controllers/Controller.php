<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Google_Client;
use Google_Service_Sheets;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    private $SPREADSHEETID = '1TEjHMDGt7x7UiJS6geFJaj_sgFoWtozf8oaoptNNRlY';

    public function index(Request $request)
    {

        $client = new Client(['base_uri' => 'https://corona.lmao.ninja/v2/']);


        $promises = [
            'world' => $client->getAsync('countries?sort=country'),
            'stats' => $client->getAsync('all')
        ];


        $responses = Promise\settle($promises)->wait();


        $worldData = $responses['world']['value']->getBody();
        $stats = $responses['stats']['value']->getBody();


        $data = [];

        $data['world'] = collect(json_decode($worldData, true))->map(function ($row) {
            return (object)[
                "id" => $row['countryInfo']['iso2'],
                "value" => $row['cases'],
                "active" => $row['active'],
                "deaths" => $row['deaths'],
                "recovered" => $row['recovered']
            ];
        })->values();

        $stats = json_decode($stats, true);

        return response()->json(compact('data', 'stats'));
    }


    public function trendAnalysis(Request $request, $country)
    {

        $country = strtolower($country);

        $client = new Client(['base_uri' => 'https://corona.lmao.ninja/v2/']);
        $promises = [
            'stats' => $client->getAsync("countries/$country"),
            'historical' => $client->getAsync("historical/$country"),
        ];

        if ($country == 'gh') {
            $promises['local'] = $client->getAsync("https://spreadsheets.google.com/feeds/list/{$this->SPREADSHEETID}/od6/public/values?alt=json");
        }

        $responses = Promise\settle($promises)->wait();

        $stats = $responses['stats']['value']->getBody();
        try {
            $historical = $responses['historical']['value']->getBody();
        } catch (\Exception $e) {
            $historical = null;
        }


        if ($country == 'gh') {
            $localData = $responses['local']['value']->getBody();
            $localCases = collect(json_decode($localData, true)['feed']['entry'])->map(function ($row) {
                return (object)[
                    "id" => $row['gsx$id']['$t'],
                    "name" => $row['gsx$name']['$t'],
                    "value" => (integer)$row['gsx$total']['$t'],
                    "active" => $row['gsx$active']['$t'],
                    "deaths" => $row['gsx$deaths']['$t'],
                    "recovered" => $row['gsx$recovered']['$t'],
                    "percentage" => $row['gsx$percentage']['$t'],
                    "color" => (integer)$row['gsx$total']['$t'] > 0 ? "#ff8726" : "#cdcdcd"
                ];
            })->sortByDesc('value')->values();

            $regional = [
                "labels" => $localCases->where('value', '>', 0)->pluck("name"),
                "data" => $localCases->where('value', '>', 0)->pluck("value")
            ];
        } else {
            $localCases = $regional = [];
        }


        if ($historical) {
            $data = json_decode($historical, true)['timeline'];
        } else {
            $data = [];
        }
        $stats = json_decode($stats, true);


        $trendData = [];
        $index = $i = 0;
        $first = true;
        if ($data) {
            foreach ($data['cases'] as $date => $value) {

                if ($value == 0 && $first) {
                    $index++;
                    continue;
                }

                $first = false;


                $formattedDate = Carbon::createFromFormat("m/d/Y", $date);
                $trendData[] = [
                    "date" => $formattedDate->format('y-m-d'),
                    "cases" => $data['cases'][$date],
                    "deaths" => $data['deaths'][$date],
                    "recovered" => $data['recovered'][$date],
                ];
            }
        }

        $pieChartData = [
            [
                "category" => "Active",
                "value" => $stats['active'],
                "color" => "#f6c23e"
            ],
            [
                "category" => "Deaths",
                "value" => $stats['deaths'],
                "color" => "#e74a3b"

            ],
            [
                "category" => "Recovered",
                "value" => $stats['recovered'],
                "color" => "#1cc88a"
            ],
        ];


        return response()->json(compact('stats', 'pieChartData', 'trendData', 'localCases', 'regional'));

    }
}
