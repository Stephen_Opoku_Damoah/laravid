# covid-19 Dashboard
A collection of JavaScript-based data visualization tools and data for depicting spread of the COVID-19.

## Important disclaimer
WE DO NOT GUARANTEE ACCURACY OF THE DATA.

Treat the data included here as a sample data, which might be (and probably is) NOT up-to-date and contain (and probably does) contain some calculational errors, especially in historical numbers.

Please note that statistics collection, especially in early days of pandemic was erratic, patchy, and not systemized.

## Data
### Source (World)
Data is provided by the [Postman COVID-19 API resource center](https://covid-19-apis.postman.com/).

### Source (Ghana Regional Breakdown)
Data for the various regions are sources from the MOH [website](https://ghanahealthservice.org/covid19/).


## Technology
This application was built using [Laravel](https://laravel.com). The admin template by [SB Admin](https://startbootstrap.com/themes/sb-admin-2/)
### Charts and maps
The chart, map and dashboard examples use amCharts 4 product; copyright amCharts. You can use and integrate the charts for free as long as you adhere to the [free use license from amCharts](https://github.com/amcharts/amcharts4#license).

### 3rd party libraries
* [amCharts](https://www.amcharts.com/)
* [ChartJs](https://jquery.com/)
* [Laravel](https://laravel.com)
* [VueJs](https://chartjs.org)

Please review their respective licenses before using in your own products.

## Contributing
If you want to contribute to this repository, create a pull request.
